﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zen.Infrasructure.Fps.Interfaces.FileParsing
{
    public interface IFileParsingConnectorResponse
    {
        bool Success { get; }
        string Code { get; }
        string Message { get; }
    }
}
