﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zen.Infrastructure.Fps.Interface.Decryption
{
    public interface IDecryptConnectorContext
    {
        string[] EncryptedFiles { get; }
        string PrivateKeyPath { get; }
        string DecryptPassword { get; }
        string DecryptOutputPath { get; }
        string PublicKeyPath { get; }
    }
}
