﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Infrastructure.Logging;

namespace Zen.Infrastructure.Fps.Interface.Decryption
{
    public interface IDecryptDestinationConnector
    {
        IDecryptConnectorResponse DecryptFile(string sourcePath, string privateKeyPath, string decryptPassword, string decryptOutputPath, string publicKeyPath, ILogger logger);
    }
}
