﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Infrastructure.FileMover;
using Zen.Infrastructure.Logging;

namespace Zen.Infrastructure.Fps.Interface.Decryption
{
    public interface IDecryptSourceConnector
    {
        IDecryptConnectorResponse GetEncryptedFiles(string sourcePath, Func<string, bool> predicate, Func<IDecryptConnectorContext, IDecryptConnectorResponse> action);
    }
}
