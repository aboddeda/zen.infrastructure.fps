﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zen.Infrastructure.Fps.Interface.Decryption
{
    public interface IDecryptTaskSettings
    {
        string DecryptSourceDirectory { get; }
        string DecryptDestinationDirectory { get; }
    }
}
