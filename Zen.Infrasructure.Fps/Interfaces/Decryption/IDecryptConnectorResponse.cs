﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zen.Infrastructure.Fps.Interface.Decryption
{
    public interface IDecryptConnectorResponse
    {
        bool Success { get; }
        string Code { get; }
        string Message { get; }
    }
}
