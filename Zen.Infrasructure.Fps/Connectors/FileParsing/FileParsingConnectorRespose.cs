﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Infrasructure.Fps.Interfaces.FileParsing;
using Zen.Infrastructure.FileMover;

namespace Zen.Infrasructure.Fps.Connectors.FileParsing
{
    public class FileParsingConnectorRespose : IFileParsingConnectorResponse
    {
        public FileParsingConnectorRespose() { }

        public bool Success { get; set; }

        public string Code { get; set; }

        public string Message { get; set; }

        public static IFileParsingConnectorResponse Okay()
        {
            return Okay("Okay");
        }

        public static IFileParsingConnectorResponse Okay(string message)
        {
            return new FileParsingConnectorRespose
            {
                Success = true,
                Code = "200",
                Message = message
            };
        }

        public static IFileParsingConnectorResponse Error(string code, string message)
        {
            return new FileParsingConnectorRespose
            {
                Success = false,
                Code = code,
                Message = message
            };
        }
    }
}
