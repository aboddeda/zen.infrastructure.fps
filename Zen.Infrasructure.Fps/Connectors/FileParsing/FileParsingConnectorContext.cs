﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Infrasructure.Fps.Interfaces.FileParsing;

namespace Zen.Infrasructure.Fps.Connectors.FileParsing
{
    public class FileParsingConnectorContext : IFileParsingConnectorContext
    {
        public FileParsingConnectorContext(string fileName, Stream stream)
        {
            FileName = fileName;
            Stream = stream;
        }

        public string FileName { get; private set; }

        public Stream Stream { get; private set; }


        public string filePath
        {
            get { throw new NotImplementedException(); }
        }
    }
}
