﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.IO;
using Org.BouncyCastle.Bcpg;
using Zen.Infrastructure.Logging;
using Zen.Infrastructure.Fps.EncryptionKeys;

namespace Zen.Infrastructure.Fps.Connectors.Decryption
{
    public class PgpDecrypt
    {
        public string encryptedFilePath;
        public string privateKeyPath;
        public char[] password;
        public string decryptOutputPath;
        public PgpEncryptionKeys pgpEncryptionKeys;
        private readonly ILogger logger;
        public PgpDecrypt(string pgpEncryptedFilePath, string pgpPrivateKeyPath, string decryptPassword, string pgpDecryptOutputPath, string publicKeyPath, ILogger logger)
        {
            encryptedFilePath = pgpEncryptedFilePath;
            decryptOutputPath = pgpDecryptOutputPath;
            password = decryptPassword.ToCharArray();
            privateKeyPath = pgpPrivateKeyPath;
            pgpEncryptionKeys = new PgpEncryptionKeys(publicKeyPath, privateKeyPath, decryptPassword);
            this.logger = logger;
        }
        public void decrypt(Stream input, string outputpath)
        {
            input = PgpUtilities.GetDecoderStream(input);
            try
            {
                PgpObjectFactory pgpObjectFactory = new PgpObjectFactory(input);
                PgpEncryptedDataList pgpEncryptedDataList;
                PgpObject pgpObject = pgpObjectFactory.NextPgpObject();
                if (pgpObject is PgpEncryptedDataList)
                {
                    pgpEncryptedDataList = (PgpEncryptedDataList)pgpObject;
                }
                else
                {
                    pgpEncryptedDataList = (PgpEncryptedDataList)pgpObjectFactory.NextPgpObject();
                }
                PgpPrivateKey privateKey = pgpEncryptionKeys.PrivateKey;
                PgpPublicKeyEncryptedData pgpPublicKeyEncryptedData = null;
                foreach (PgpPublicKeyEncryptedData pgpPublicKeyEncryptedDataItem in pgpEncryptedDataList.GetEncryptedDataObjects())
                {
                    if (privateKey != null)
                    {
                        pgpPublicKeyEncryptedData = pgpPublicKeyEncryptedDataItem;
                        break;
                    }
                }
                Stream clear = pgpPublicKeyEncryptedData.GetDataStream(privateKey);
                PgpObjectFactory plainFact = new PgpObjectFactory(clear);
                PgpObject pgpObjectMessage = plainFact.NextPgpObject();
                if (pgpObjectMessage is PgpCompressedData)
                {
                    PgpCompressedData pgpCompressedData = (PgpCompressedData)pgpObjectMessage;
                    Stream pgpCompressedDataStream = pgpCompressedData.GetDataStream();
                    PgpObjectFactory pgpCompressedDataStreamObjectFactory = new PgpObjectFactory(pgpCompressedDataStream);
                    pgpObjectMessage = pgpCompressedDataStreamObjectFactory.NextPgpObject();
                    if (pgpObjectMessage is PgpOnePassSignatureList)
                    {
                        pgpObjectMessage = pgpCompressedDataStreamObjectFactory.NextPgpObject();
                        PgpLiteralData pgpLiteralData = null;
                        pgpLiteralData = (PgpLiteralData)pgpObjectMessage;
                        Stream output = File.Create(outputpath + "\\" + pgpLiteralData.FileName);
                        Stream pgpLiteralDataInputStream = pgpLiteralData.GetInputStream();
                        Streams.PipeAll(pgpLiteralDataInputStream, output);
                    }
                    else
                    {
                        PgpLiteralData pgpLiteralData = null;
                        pgpLiteralData = (PgpLiteralData)pgpObjectMessage;
                        Stream output = File.Create(outputpath + "\\" + pgpLiteralData.FileName);
                        Stream pgpLiteralDataInputStream = pgpLiteralData.GetInputStream();
                        Streams.PipeAll(pgpLiteralDataInputStream, output);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Information("Decryption Failed...." + ex.Message);
            }
        }
    }
}
