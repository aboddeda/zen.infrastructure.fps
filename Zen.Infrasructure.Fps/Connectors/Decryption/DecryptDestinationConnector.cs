﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Infrastructure.Fps.Interface.Decryption;
using Zen.Infrastructure.Logging;

namespace Zen.Infrastructure.Fps.Connectors.Decryption
{
    public class DecryptDestinationConnector : IDecryptDestinationConnector
    {

        public DecryptDestinationConnector()
        {
        }

        public IDecryptConnectorResponse DecryptFile(string sourcePath, string privateKeyPath, string decryptPassword, string decryptOutputPath, string publicKeyPath, ILogger logger)
        {
            
            try
            {
                var filename = string.Empty;
                var fileExtension = string.Empty;
                var fileNameWithoutExtension = string.Empty;
                bool isSucess = false;

                string[] encryptedFiles = Directory.GetFiles(sourcePath);


                foreach (var file in encryptedFiles)
                {

                    filename = Path.GetFileName(file);
                    fileExtension = Path.GetExtension(file);
                    fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file);

                    if (!fileExtension.EndsWith(".processing"))
                    {
                        PgpDecrypt pgpDecrypt = new PgpDecrypt(file,
                                                         privateKeyPath,
                                                         decryptPassword,
                                                         decryptOutputPath,
                                                         publicKeyPath, logger);
                        logger.Debug("Decrypting file: {fileName}.", filename);
                        FileStream fsDecryprt = File.Open(file, FileMode.Open);
                        pgpDecrypt.decrypt(fsDecryprt, decryptOutputPath);
                        logger.Debug("Decrypted file and saved at : {0}.", decryptOutputPath);
                        isSucess = true;
                    }
                }
                return DecryptConnectorResponse.Okay();
            }
            catch (Exception ex)
            {
                logger.Information("Decryption Failed..." + ex.Message);
                return DecryptConnectorResponse.Error(ex.Message, ex.Message);
            }
        }

    }
}
