﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Infrastructure.FileMover;
using Zen.Infrastructure.FileMover.Connectors.SshFtp;
using Zen.Infrastructure.Fps.Interface;
using Zen.Infrastructure.Fps.Interface.Decryption;
using Zen.Infrastructure.Logging;

namespace Zen.Infrastructure.Fps.Connectors.Decryption
{
    public class DecryptSourceConnector : IDecryptSourceConnector
    {

        public DecryptSourceConnector()
        {
        }

        public IDecryptConnectorResponse GetEncryptedFiles(string sourcePath, Func<string, bool> predicate, Func<IDecryptConnectorContext, IDecryptConnectorResponse> action)
        {
            DecryptConnectorContext decryptConnectorContext;
            IDecryptConnectorResponse decryptConnectorResponse;

            string[] encryptedFilesPath = Directory.GetFiles(ConfigurationManager.ConnectionStrings["EncryptedFilePath"].ConnectionString);
            string privateKeyPath = ConfigurationManager.ConnectionStrings["PrivateKeyPath"].ConnectionString;
            string decryptPassword = ConfigurationManager.ConnectionStrings["DecryptPassword"].ConnectionString;
            string decryptOutputPath = ConfigurationManager.ConnectionStrings["DecryptOutputPath"].ConnectionString;
            string publicKeyPath = ConfigurationManager.ConnectionStrings["PublicKeyPath"].ConnectionString;
            string destination = ConfigurationManager.ConnectionStrings["Destination"].ConnectionString;

            decryptConnectorContext = new DecryptConnectorContext(encryptedFilesPath, privateKeyPath, decryptPassword, decryptOutputPath, publicKeyPath);
            decryptConnectorResponse = action(decryptConnectorContext);

            return decryptConnectorResponse;
        }



    }
}
