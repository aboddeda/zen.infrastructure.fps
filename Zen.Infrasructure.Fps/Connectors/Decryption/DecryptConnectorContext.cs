﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Infrastructure.Fps.Interface.Decryption;

namespace Zen.Infrastructure.Fps.Connectors.Decryption
{
    public class DecryptConnectorContext : IDecryptConnectorContext
    {
        public DecryptConnectorContext(string[] encryptedFiles, string privateKeyPath, string decryptPassword, string decryptOutputPath, string publicKeyPath)
        {
            EncryptedFiles = encryptedFiles;
            PrivateKeyPath = privateKeyPath;
            DecryptPassword = decryptPassword;
            DecryptOutputPath = decryptOutputPath;
            PublicKeyPath = publicKeyPath;
        }

        public string[] EncryptedFiles { get; private set; }
        public string PrivateKeyPath { get; private set; }
        public string DecryptPassword { get; private set; }
        public string DecryptOutputPath { get; private set; }
        public string PublicKeyPath { get; private set; }
        
    }
}
