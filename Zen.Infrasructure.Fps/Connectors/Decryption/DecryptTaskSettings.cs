﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Infrastructure.Fps.Interface.Decryption;

namespace Zen.Infrastructure.Fps.Connectors.Decryption
{
    public class DecryptTaskSettings : IDecryptTaskSettings
    {
        public string DecryptSourceDirectory { get; set; }
        public string DecryptDestinationDirectory { get; set; }
    }
}
