﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Infrastructure.Fps.Interface.Decryption;

namespace Zen.Infrastructure.Fps.Connectors.Decryption
{
    public class DecryptConnectorResponse : IDecryptConnectorResponse
    {
        public DecryptConnectorResponse() { }

        public bool Success { get; set; }

        public string Code { get; set; }

        public string Message { get; set; }

        public static IDecryptConnectorResponse Okay()
        {
            return Okay("Okay");
        }

        public static IDecryptConnectorResponse Okay(string message)
        {
            return new DecryptConnectorResponse
            {
                Success = true,
                Code = "200",
                Message = message
            };
        }

        public static IDecryptConnectorResponse Error(string code, string message)
        {
            return new DecryptConnectorResponse
            {
                Success = false,
                Code = code,
                Message = message
            };
        }
    }
}
